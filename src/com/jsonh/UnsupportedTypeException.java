package com.jsonh;

public class UnsupportedTypeException extends RuntimeException {
	public UnsupportedTypeException(Exception e) {
		super(e);
	}

	public UnsupportedTypeException() {
		super();
	}

	private static final long serialVersionUID = -3908919293024261189L;
}

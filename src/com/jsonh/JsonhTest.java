package com.jsonh;

import static org.junit.Assert.*;

import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

public class JsonhTest {

	@Test
	public void testUnpack() throws Exception {
		JSONArray src = new JSONArray();
		
		src.put(2);
		src.put("A");
		src.put("B");
		src.put(12);
		src.put("S");
		src.put(-1);
		src.put("AA");
		src.put(9);
		src.put("XX");
		
		JSONArray res = Jsonh.unpack(src);
		assertEquals(3, res.length());
		
		JSONObject o = res.getJSONObject(0);
		assertEquals(12, o.getInt("A"));
		assertEquals("S", o.getString("B"));
		
		o = res.getJSONObject(1);
		assertEquals(-1, o.getInt("A"));
		assertEquals("AA", o.getString("B"));
		
		o = res.getJSONObject(2);
		assertEquals(9, o.getInt("A"));
		assertEquals("XX", o.getString("B"));
	}

	@Test
	public void testPack() throws Exception {
		JSONArray src = new JSONArray();
		JSONObject o = new JSONObject();
		o.put("A", 12);
		o.put("B", "S");
		src.put(o);
		
		o = new JSONObject();
		o.put("A", -1);
		o.put("B", "AA");
		src.put(o);
		
		o = new JSONObject();
		o.put("A", 9);
		o.put("B", "XX");
		src.put(o);
		
		JSONArray res = Jsonh.pack(src);
		assertEquals(9, res.length());
		assertEquals(2, res.getInt(0));

		HashMap<String,Integer> kidx = new HashMap<String,Integer>();
		kidx.put(res.getString(1), 0);
		kidx.put(res.getString(2), 1);
		
		assertEquals(12, res.getInt(3+kidx.get("A")));
		assertEquals("S", res.getString(3+kidx.get("B")));
		assertEquals(-1, res.getInt(5+kidx.get("A")));
		assertEquals("AA", res.getString(5+kidx.get("B")));
		assertEquals(9, res.getInt(7+kidx.get("A")));
		assertEquals("XX", res.getString(7+kidx.get("B")));
	}

}

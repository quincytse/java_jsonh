package com.jsonh;

public class HeterogeneousListException extends RuntimeException {
	public HeterogeneousListException(Exception e) {
		super(e);
	}

	public HeterogeneousListException() {
		super();
	}

	private static final long serialVersionUID = -4211095689774312590L;
}

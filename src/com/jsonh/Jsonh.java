package com.jsonh;

import java.util.Iterator;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * This class compresses and decompresses between a Homogeneous JSONArray 
 * and a JSONH-packed JSONArray. For details of the algorithm, see
 * <a href="https://github.com/WebReflection/JSONH">WebReflection's GitHub page</a>. 
 *  
 * @author Quincy Tse <quincy.tse@gmail.com>
 * @see https://github.com/WebReflection/JSONH
 */
public class Jsonh {
	/**
	 * This method decompresses a JSONH-packed JSONArray back to a 
	 * homogeneous JSONArray of JSONObjects.
	 * 
	 * @throws UnsupportedListException If the input is not a valid
	 * 			JSONH JSONArray.
	 * @param packed The input JSONArray
	 * @return The decompressed JSONArray
	 */
	public static JSONArray unpack(JSONArray packed) {
		if (null == packed) {
			return packed;
		}
		
		int packedLen = packed.length();
		int rowLen;
		
		try {
			if (packedLen < 1 || packed.getInt(0) < 0) {
				throw new UnsupportedListException();
			} else {
				rowLen = packed.getInt(0);
			}
		} catch (JSONException e) {
			throw new UnsupportedListException(e);
		}
		
		if (packedLen % rowLen != 1) {
			throw new UnsupportedListException();
		}
		
		int nEntries = packedLen/rowLen;
		JSONArray res = new JSONArray();
		
		for (int i = 1; i < nEntries; i++) {
			JSONObject o = new JSONObject();
			for (int j = 0; j < rowLen; j++) {
				try {
					o.put(packed.getString(j+1), packed.get(i*rowLen + j + 1));
				} catch (JSONException e) {
					throw new UnsupportedListException(e);
				}
			}
			res.put(o);
		}
		
		return res;
	}
	
	/**
	 * This method compresses a homogeneous JSONArray into a 
	 * JSONH-packed JSONArray.
	 * 
	 * @throws UnsupportedListException If the input is not homogeneous.
	 * @param packed The input JSONArray
	 * @return The compressed JSONArray
	 */
	public static JSONArray pack(JSONArray orig) {
		if (null == orig) {
			return orig;
		}
		
		if (orig.length() < 1) {
			throw new UnsupportedListException();
		}
		
		int rowLen;
		try {
			rowLen = orig.getJSONObject(0).length();
		} catch (JSONException e) {
			throw new UnsupportedListException(e);
		}
		if (rowLen < 1) {
			throw new UnsupportedListException();
		}
		
		JSONArray res = new JSONArray();
		res.put(rowLen);
		
		String[] keyIdx = new String[rowLen];
		
		{
			int i = 0;
		
			try {
				for (@SuppressWarnings("rawtypes")Iterator it = orig.getJSONObject(0).keys();
					it.hasNext();) {
					keyIdx[i] = (String)it.next();
					res.put(keyIdx[i++]);
				}
			} catch (JSONException e) {
				throw new UnsupportedListException(e);
			}
		}
		
		for (int i = 0; i < orig.length(); i++) {
			for (int j = 0; j < rowLen; j++) {
				try {
					res.put(orig.getJSONObject(i).get(keyIdx[j]));
				} catch (JSONException e) {
					throw new UnsupportedListException(e);
				}
			}
		}
		
		return res;
	}
}

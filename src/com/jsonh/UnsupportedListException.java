package com.jsonh;

public class UnsupportedListException extends RuntimeException {
	public UnsupportedListException(Exception e) {
		super(e);
	}

	public UnsupportedListException() {
		super();
	}

	private static final long serialVersionUID = -2335633195058826012L;

}
